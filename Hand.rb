require './Card'

#  Represents hand used to play BlackJack
class Hand
	# Hand id
  attr_accessor :id
  # Cards in hand
  attr_accessor :cards
  # Busted flag
  attr_accessor :busted
  # Bet on hand
  attr_accessor :bet
  # Double down flag
  attr_accessor :doubled

  # Initialize hand with a new array for cards
  def initialize
	self.cards = Array.new
  end

  # Add a card to the hand
  def addCard(c)
  	self.cards.insert(self.cards.length, c)
  end

  # Get a card based on position from the hand
  def getCard(i)

  	if i > self.cards.length
  		return nil
  	else
  		return self.cards[i]
  	end
  end

  # Get number of cards in hand
  def getCount
      return hand.length;
  end

  # Get value of calling hand
  #
  # * loop over all the cards in the hand to obtain value
  # * Decide most convenient value for ace
  def getValue
  	# accumulative card value
  	value = 0
  	# ace flag for deciding best way to use ace
  	ace = false
  	for card in self.cards
  		# If value is higher than 10 it is a face card, use 10
  		curr_val = card.rank.to_i >= 10 ? 10 : card.rank.to_i
  		# set ace flag if we have an ace in the hand
  		if curr_val == 1
  			ace = true
  		end
  		value += curr_val
  	end

  	if ace == true
  		if value + 10 <= 21
  			value += 10
  		end
  	end
  	# return the value
  	value
  end



end

 