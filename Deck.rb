require './Card'
#  Represents deck used to play black jack
class Deck

  # Cards in deck
  attr_accessor :cards
  # Usded cards from deck
  attr_accessor :used

# initialize cards array 
  def initialize
    self.cards = (1..52).to_a.collect { |id| Card.new(id) }
    #initialize used cards
    self.used = 0
  end

  # Shuffle deck of cards
  def shuffle
    n = self.cards.length
    for i in 0...n
      r = rand(n-i)+i
      self.cards[r], self.cards[i] = self.cards[i], self.cards[r]
    end
    self
  end

  # Returns a card from the Deck. 
  def dealCard       
        if self.used == 52
           self.shuffle
           self.used = 0
        end
        self.used += 1
        card = self.cards[used - 1]
 end
end
