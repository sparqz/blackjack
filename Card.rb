#  Represents cards used to play blackjack
class Card

  # Card suits
  CARD_SUITS = %w(Hearts Diamonds Clubs Spades)
  # Card ranks, numerical value
  CARD_RANKS = %w(1 2 3 4 5 6 7 8 9 10 11 12 13)
  # Card (picture) value for informative 
  CARD_VALUE = %w(ACE 2 3 4 5 6 7 8 9 10 Jack Queen King)

  # Create reader and writer methods for rank and suit 
  attr_accessor :rank, :suit

  # Initialize a deck of cards
  def initialize(id)
  	# use mod wrap around to ensure valid ranks and suits
    self.rank = CARD_RANKS[id % 13]
    self.suit = CARD_SUITS[id % 4]
  end

  # Class method to get the value of cards
  def self.cardValue(i)
  	CARD_VALUE[i.to_i - 1]
  end

end