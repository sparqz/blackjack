
require './Card'
require './Deck'
require './Hand'
require './BlackJack'

#########################################################################################################################################

# Main Program Driver

#########################################################################################################################################

# get instance of BlackJack class
blackjack = BlackJack.new
# Make Deck 
blackjack.deck = Deck.new
# Array for players hands
hands = Array.new
# number of players (from user input)
num_players =  ""
# bet for each player (from user input)
bet = 0
# Create hash table with players and money
blackjack.players = Hash.new
# Reapeat until a valid interger is given for the number of players
loop do
	puts "Please enter number of players on table between 1 and 7 \n"
	STDOUT.flush  
	num_players = gets.chomp  
	#break only if the input is valid
	break if num_players.is_number? && num_players.to_i > 0 && num_players.to_i < 8 
end

#Initialize players
blackjack.initialize_players( num_players.to_i)
loop do
	playercount = 0
	# Collect bets from each player
	blackjack.players.each do |key, value|
		# Validate that player has sufficient funds to bet
		if value.to_i > 0
			playercount += 1
		    hand = blackjack.makeHand( key, value )
		    hands.push(hand);
		end
	end
	# Close table if no players are left with money
	if playercount == 0
		puts "No more players with money at table.  Thank you for playing BlackJack!"
		exit
	end
	# Play a round of Black Jack
	blackjack.play(hands)
	# Clear hands
	hands.clear
	# Prompt for anotehr round
	puts "\n\n\ntype \"Y\" to play another round of Black Jack or any other key + Enter to quit\n"
	STDOUT.flush  
	play = gets.chomp.upcase
	# Break to end game
	break if  play != "Y"
end
