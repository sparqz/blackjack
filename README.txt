README

included:
All source files (.rb)
this README
Complete Documentation (/doc)

===BlackJack===

This is the ruby implementation of  the game of blackjack. It employs a command-line interface. The program begins by asking how many players are at the table, starts each player with $1000, and allow the players to make any integer bet for each deal.

The program implements the core blackjack rules, i.e. players can choose to hit until they go over 21, the dealer hits on 16 and stay on 17, etc. It also supports doubling-down and splitting (split hands are like fresh hands and can be split again).  

All sources are provided,  the instruction to run the program is as follows:

1) Navigate to the project root directory
2) In the command line run the command ‘ruby Main.rb’
3) Follow the command line instructions to play the game.
