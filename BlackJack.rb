require './Card'
require './Deck'
require './Hand'

#  Methods and Attributes to play BlackJack
class BlackJack

	# hash to store players
	attr_accessor :players

	# Deck of cards to play BlackJack
	attr_accessor :deck

	# Play a round of BlackJack
	#
	# * Takes a hands as argument
	# * Interates over each hand in hands
	# * Allows player to double down, split, hit or stand
	# * Wehn players are done, dealer deals himself additional cards
	# * When dealer is finished, evaluates each hand against dealer
	def play(hands)
		deck.shuffle
		dealer = Hand.new
		dealer.addCard( deck.dealCard )
		dealer.addCard( deck.dealCard )
		index = 0
		while index < hands.size
			hand = hands[index] 
			puts "\n\n\nPlayer #{hand.id}'s turn"
			puts "Dealer is showing card: #{Card.cardValue(dealer.getCard(0).rank)} of #{dealer.getCard(0).suit}\n"	
			puts "Your total is #{hand.getValue} with cards:  #{Card.cardValue(hand.cards[0].rank)} of #{hand.cards[0].suit} and  #{Card.cardValue(hand.cards[1].rank)} of #{hand.cards[1].suit}"
			# Call double down subroutine
			hand, double = doubleDown( hand )
			if double == "D"
				index += 1
				next
			end
			# Call split subroutine
			hands , split = split( hand , hands , index)
			if split == "Y"
				next
			end
			# Player continously hits (until busts) or stands
			hand = hitOrStand( hand )
			index += 1
		end
		# Dealer draws aditional cards
		dealer = dealerDraw( dealer)
		# Check player hands against dealer hand
		evaluate( hands , dealer )
	end


	# Method to split a hand of identically ranked cards
	#
	# * Takes a hand as an argument
	# * update hands with data from the "parent" hand 
	# * and a dealt card from the deck
	# * Splits hand into two hands and deals a card to each split hand
	def split( hand, hands, index )
		split = ""
		if ( hand.cards[0].rank == hand.cards[1].rank ) && ( self.players[hand.id].to_i - hand.bet.to_i >= 0 )
			puts "Would you like to split?, Type \"Y\" to split or hit Enter to ignore\n"
			STDOUT.flush  
			split = gets.chomp.upcase 
			if split == "Y"
				h1 = Hand.new
				h2 = Hand.new
				arr = Array.new
				h1.addCard( hand.cards[0] )
				h1.addCard( self.deck.dealCard )
				h1.id = hand.id
				h1.bet = hand.bet
				h2.addCard( hand.cards[1]  )
				h2.addCard( self.deck.dealCard )
				h2.id = hand.id
				h2.bet = hand.bet
				self.players[hand.id] = self.players[hand.id].to_i - hand.bet.to_i;
				arr.push( h1 )
				arr.push( h2 )
				hands.delete_at(index)
				hands.insert(index, arr[0], arr[1])
			end
		end
		
		return hands , split
	end


	# Method to allow player to double down hand
	#
	# * Takes a hand as an argument
	# * If the player chooses to double hand
    # * Makes a hit on the hand and doubles the player's bet
	def doubleDown( hand )
		double = ""
		if self.players[hand.id].to_i - hand.bet.to_i >= 0
			puts "\n\nWould you like to Double Down?, Type \"D\" to Double down or hit Enter to ignore\n"
			STDOUT.flush  
			double = gets.chomp.upcase 
			if double == "D"
				hand.doubled = true
				hand = hit( hand, false )
				self.players[hand.id] = self.players[hand.id].to_i - hand.bet.to_i;
				hand.bet *= 2
			end
		end
		return hand , double
	end


	# Method to hit on the hand
	#
	# * Takes a hand as an argument and performs a hit
	# * Takes isdealer flag as an argument correctly form busted message for dealer or player
	def hit( hand, isdealer ) 
		card = self.deck.dealCard
		puts "New card is: #{Card.cardValue(card.rank)} of #{card.suit}"
		hand.addCard(card)
		puts "New total is #{hand.getValue}"
		if hand.getValue > 21
			if !isdealer 
				puts "Player #{hand.id} busted"
			else
				puts "Dealer busted"
			end
			hand.busted = true;
		end
		return hand
	end

	
	# Method to evaluate hands against the dealer
	#
	# * Takes array of player hands  and dealer hand as argument
	# * Evaluates value of players hands against dealer
	# * Announces if the player won or lost, the players hand value and remaining money
	def evaluate( hands, dealer)
		hands.each do |hand|
			if (hand.getValue <= dealer.getValue  && dealer.busted != true ) || hand.busted == true 
				# player has lost
				puts "\n\nPlayer #{hand.id} has lost with total hand value of #{hand.getValue} vs #{dealer.getValue} "
				puts "Player #{hand.id} new total : $#{self.players[hand.id]}"	
			else
				# player has won
				puts "\n\nPlayer #{hand.id} has won with total hand value of #{hand.getValue} vs #{dealer.getValue} "
				self.players[hand.id] = (self.players[hand.id].to_i + (2 * hand.bet.to_i)).to_s
				puts "Player #{hand.id} new total is: $#{self.players[hand.id]}"
			end
		end
	end


	# Method that draws additional dealer cards from the deck
	#
	# * Takes dealer hand as an argument, deals cards until
	# * dealer's hand is above 17 or busts.
	def dealerDraw( dealer)
		puts "\n\nDealer's turn to draw, dealer's down card was #{Card.cardValue(dealer.getCard(1).rank)} of #{dealer.getCard(1).suit}"
		while dealer.getValue < 17
			dealer = self.hit( dealer, true )
		end
		puts "Dealers  total  = #{dealer.getValue}"
		return dealer
	end

	# Hit or Stand action to be taken by user
	#
	# * Takes hand as argument and allows player to hit or stand
	def hitOrStand( hand )
			action = ""
			while true
				loop do
					puts "\n\nType \"H\" to HIT and \"S\" to STAND"
					STDOUT.flush  
					action = gets.chomp.upcase  
					break if action == "H" || action == "S"
				end
				if action == "S"
					break
				else
					hand = hit( hand, false )
					if hand.busted == true
						break
					end
				end
			end
			return hand
	end


	# Initialize Players
	# 
	# * Receives number of players on table
	# * Creates players hash table with starting funds of $1000
	def initialize_players ( total_Players)
		# Initialize each player wiht $1000
		count = 0
		while count <  total_Players
			self.players[count+1] = 1000
			count += 1
		end
	end


	# Initialize hand for player
	# 
	# * Receives player id, player total and bet amount
	# * Creates hand and deals two cards
	def makeHand( id , total )
		# Get the bet from the player
		bet = 0
		loop do
			puts "\n\n\nPlayer #{id} please enter your bet \n"
			STDOUT.flush  
			bet = gets.chomp 
			break if  bet.is_number? && bet.to_i > 0 && bet.to_i <= total.to_i
		end
		hand = Hand.new
		hand.id = id
		hand.bet = bet.to_i
		self.players[id] = total.to_i - bet.to_i;
		self.deck.shuffle
		hand.addCard( self.deck.dealCard )
		hand.addCard( self.deck.dealCard )
		return hand
	end

end

#########################################################################################################################################
# String class method definition
class String
  # Simple declaration of is_number method in string class (this is for validating that user inputs are numbers)
  def is_number?
    true if Integer(self) rescue false
  end
end


























